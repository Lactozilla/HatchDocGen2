#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <algorithm>

enum DefType {
    DEF_FUNCTION,
    DEF_METHOD,
    DEF_FIELD,
    DEF_CLASS_FIELD,
    DEF_ENUM,
    DEF_CONSTANT,
    DEF_GLOBAL_VAR,

    DEF_LAST
};

bool is_field_type(DefType type) {
    return type == DEF_FIELD || type == DEF_CLASS_FIELD;
}

const char* defTypeNames[][2] = {
    { "functions", "Class methods" },
    { "methods", "Instance methods" },
    { "fields", "Instance fields" },
    { "class fields", "Class fields" },
    { "enums", "Enums" },
    { "constants", "Constants" },
    { "globals", "Globals" }
};

std::string markerDefStart = "/***";
std::string markerDefEnd = "*/";

std::string markerMethod = "* \\method";
std::string markerConstant = "* \\constant";
std::string markerEnum = "* \\enum";
std::string markerGlobal = "* \\global";
std::string markerField = "* \\field";
std::string markerClassField = "* \\classfield";

std::pair<std::string, DefType> markers[] = {
    std::make_pair(markerMethod, DEF_METHOD),
    std::make_pair(markerField, DEF_FIELD),
    std::make_pair(markerClassField, DEF_CLASS_FIELD),
    std::make_pair(markerEnum, DEF_ENUM),
    std::make_pair(markerConstant, DEF_CONSTANT),
    std::make_pair(markerGlobal, DEF_GLOBAL_VAR)
};

std::string markerDesc = "* \\desc";
std::string markerParam = "* \\param";
std::string markerReturn = "* \\return";
std::string markerParamOpt = "* \\paramOpt";
std::string markerType = "* \\type";
std::string markerDefault = "* \\default";
std::string markerNamespace = "* \\ns ";

class DocDef {
public:
    DefType Type;

    std::string Title = "";
    std::string Description = "";
    std::string Namespace = "";

    std::string getTitle() {
        if (is_field_type(Type) || Type == DEF_METHOD) {
            if (Type == DEF_FIELD || Type == DEF_METHOD) {
                std::string nsName(Namespace);
                std::transform(nsName.begin(), nsName.end(), nsName.begin(), [](unsigned char c) {
                    return std::tolower(c);
                });
                return nsName + "." + Title;
            }

            return Namespace + "." + Title;
        }

        return Title;
    }

    std::string getNameForHTML() {
        std::string name(getTitle());
        std::replace(name.begin(), name.end(), '.', '_');
        return name;
    }

    std::string getHref() {
        return "Reference_" + std::string(defTypeNames[Type][0]) + "_" + getNameForHTML();
    }
};

class ParamDef {
public:
    std::string Label = "";
    bool Optional = false;

    ParamDef(std::string label, bool optional) {
        Label = label;
        Optional = optional;
    }
};

class FunctionDef : public DocDef {
public:
    std::vector<ParamDef> Params;
    std::string Returns = "";

    FunctionDef() {
        Type = DEF_FUNCTION;
    }
};

class EnumDef : public DocDef {
public:
    std::vector<ParamDef> Params;
    std::string Prefix = "";

    EnumDef() {
        Type = DEF_ENUM;
    }
};

class ConstantDef : public DocDef {
public:
    std::string ValueType = "";

    ConstantDef() {
        Type = DEF_CONSTANT;
    }
};

class FieldDef : public ConstantDef {
public:
    std::string DefaultValue = "";

    FieldDef() {
        Type = DEF_FIELD;
    }
};

class NamespaceInfo {
public:
    std::string Name;
    bool IsEnumNamespace = false;
    std::vector<std::vector<DocDef*>> DocsPerDef;
};
std::map<std::string, NamespaceInfo*> allNamespaces;
std::map<std::string, std::string> allHref;

std::string getNamespaceHref(std::string nsName) {
    return "Reference_" + nsName;
}

class DocGroup {
public:
    std::vector<DocDef*> DocList;
    std::map<std::string, std::vector<DocDef*>*> Namespaces;
    std::vector<std::string> NamespaceList;
    unsigned Count, HasDesc;

    void AddNamespace(DocDef* def) {
        std::string nsName = def->Namespace;
        if (nsName == "")
            return;

        std::vector<DocDef*> *ns;

        // Check if this namespace exists
        if (!Namespaces.count(nsName)) {
            ns = new std::vector<DocDef*>;
            Namespaces[nsName] = ns;
            NamespaceList.push_back(nsName);
        }

        ns = Namespaces[nsName];
        ns->push_back(def);
    }

    void AddPrefix(EnumDef* def) {
        std::string prefix = def->Prefix;
        if (prefix == "")
            return;

        std::vector<DocDef*> *ns;

        // Check if this namespace exists
        if (!Namespaces.count(prefix)) {
            ns = new std::vector<DocDef*>;
            Namespaces[prefix] = ns;
            NamespaceList.push_back(prefix);
        }

        ns = Namespaces[prefix];
        ns->push_back(def);

        if (!allNamespaces.count(prefix)) {
            allNamespaces[prefix] = new NamespaceInfo();
            allNamespaces[prefix]->IsEnumNamespace = true;
            allNamespaces[prefix]->DocsPerDef.resize(DEF_LAST);
            allHref[prefix] = getNamespaceHref(prefix);
        }
        allNamespaces[prefix]->DocsPerDef[def->Type].push_back(def);
    }
};
std::vector<DocGroup*> docLists;

void add_namespace(DocGroup* group, DocDef* def) {
    std::string nsName = def->Namespace;
    if (nsName == "") {
        if (def->Type == DEF_ENUM)
            group->AddPrefix(static_cast<EnumDef*>(def));
        return;
    }

    group->AddNamespace(def);

    if (!allNamespaces.count(nsName)) {
        allNamespaces[nsName] = new NamespaceInfo();
        allNamespaces[nsName]->DocsPerDef.resize(DEF_LAST);
        allHref[nsName] = getNamespaceHref(nsName);
    }

    allNamespaces[nsName]->DocsPerDef[def->Type].push_back(def);
}

std::vector<std::string> get_file_lines(std::string filename) {
    std::vector<std::string> lines;
    std::ifstream stream;

    stream.open(filename.c_str());

    if (!stream.is_open() || stream.bad())
        throw "couldn't open " + filename;

    for (std::string line; std::getline(stream, line ); )
        lines.push_back(line);

    return lines;
}

std::string string_trim_start(std::string str) {
    std::string s = str;
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
    return s;
}

std::string string_trim_end(std::string str) {
    std::string s = str;
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
    return s;
}

std::string string_trim(std::string str) {
    return string_trim_start(string_trim_end(str));
}

bool string_starts_with(std::string_view str, std::string_view prefix) {
    if (str.size() < prefix.size())
        return false;

    return str.compare(0, prefix.size(), prefix) == 0;
}

bool string_ends_with(std::string_view str, std::string_view suffix) {
    if (str.size() < suffix.size())
        return false;

    return str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

std::string get_marker(std::string marker, std::string line) {
    return string_trim(line.substr(marker.length()));
}

std::string get_multiline_marker(std::string marker, std::string line, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    std::string result = string_trim(line.substr(marker.length()));
    while (string_ends_with(result, "\\"))  {
        if (i >= numLines)
            break;
        result = result.substr(0, result.length() - 1) + string_trim(sourceLines[++i]);
    }
    return result;
}

bool parse_base_def_marker(DocDef* def, std::string line, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    // desc
    if (string_starts_with(line, markerDesc)) {
        def->Description = get_multiline_marker(markerDesc, line, sourceLines, i, numLines);
        return true;
    }
    // ns
    else if (string_starts_with(line, markerNamespace)) {
        def->Namespace = get_marker(markerNamespace, line);
        return true;
    }

    return false;
}

FunctionDef* parse_function_def(std::string title, DefType type, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    FunctionDef* def = new FunctionDef();
    def->Title = title;
    def->Type = type;
    for (; i < numLines; i++) {
        std::string line = string_trim(sourceLines[i]);
        if (string_starts_with(line, markerDefEnd))
            break;
        if (parse_base_def_marker(def, line, sourceLines, i, numLines))
            continue;
        // paramOpt
        if (string_starts_with(line, markerParamOpt)) {
            ParamDef paramOpt(get_multiline_marker(markerParamOpt, line, sourceLines, i, numLines), true);
            def->Params.push_back(paramOpt);
        }
        // param
        else if (string_starts_with(line, markerParam)) {
            ParamDef param(get_multiline_marker(markerParam, line, sourceLines, i, numLines), false);
            def->Params.push_back(param);
        }
        // return
        else if (string_starts_with(line, markerReturn))
            def->Returns = get_multiline_marker(markerReturn, line, sourceLines, i, numLines);
    }
    return def;
}

DocDef* parse_generic_def(std::string title, DefType type, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    DocDef* def = new DocDef();
    def->Title = title;
    def->Type = type;

    for (; i < numLines; i++) {
        std::string line = string_trim(sourceLines[i]);
        if (string_starts_with(line, markerDefEnd))
            break;
        parse_base_def_marker(def, line, sourceLines, i, numLines);
    }

    return def;
}

EnumDef* parse_enum_def(std::string title, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    EnumDef* def = new EnumDef();

    def->Title = title;

    for (; i < numLines; i++) {
        std::string line = string_trim(sourceLines[i]);
        if (string_starts_with(line, markerDefEnd))
            break;
        parse_base_def_marker(def, line, sourceLines, i, numLines);
    }

    size_t pos = title.find('_');
    if (pos != std::string::npos)
        def->Prefix = title.substr(0, pos + 1) + "*";

    return def;
}

ConstantDef* parse_constant_def(std::string title, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    ConstantDef* def = new ConstantDef();

    def->Title = title;

    for (; i < numLines; i++) {
        std::string line = string_trim(sourceLines[i]);
        if (string_starts_with(line, markerDefEnd))
            break;
        if (parse_base_def_marker(def, line, sourceLines, i, numLines))
            continue;
        // type
        if (string_starts_with(line, markerType))
            def->ValueType = get_marker(markerType, line);
    }

    return def;
}

FieldDef* parse_field_def(std::string title, DefType type, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    FieldDef* def = new FieldDef();

    def->Title = title;
    def->Type = type;

    for (; i < numLines; i++) {
        std::string line = string_trim(sourceLines[i]);
        if (string_starts_with(line, markerDefEnd))
            break;
        if (parse_base_def_marker(def, line, sourceLines, i, numLines))
            continue;
        // type
        if (string_starts_with(line, markerType))
            def->ValueType = get_marker(markerType, line);
        // default
        else if (string_starts_with(line, markerDefault))
            def->DefaultValue = get_marker(markerDefault, line);
    }

    return def;
}

DocDef* parse_def(std::string title, DefType type, std::vector<std::string>& sourceLines, size_t& i, size_t numLines) {
    switch (type) {
    case DEF_FUNCTION:
    case DEF_METHOD:
        return parse_function_def(title, type, sourceLines, i, numLines);
    case DEF_ENUM:
        return parse_enum_def(title, sourceLines, i, numLines);
    case DEF_CONSTANT:
        return parse_constant_def(title, sourceLines, i, numLines);
    case DEF_FIELD:
    case DEF_CLASS_FIELD:
        return parse_field_def(title, type, sourceLines, i, numLines);
    default:
        return parse_generic_def(title, type, sourceLines, i, numLines);
    }
}

void read_file(std::string sourceFile) {
    std::vector<std::string> sourceLines;
    size_t numLines;

    try {
        sourceLines = get_file_lines(sourceFile);
        numLines = sourceLines.size();
    } catch (std::string err) {
        fprintf(stderr, "%s\n", err.c_str());
        return;
    }

    if (numLines == 0)
        return;

    for (size_t i = 0; i < numLines; i++) {
        std::string line = string_trim(sourceLines[i]);

        if (!string_starts_with(line, markerDefStart))
            continue;

        DocDef* def = nullptr;
        for (i++; i < numLines; i++) {
            line = string_trim(sourceLines[i]);
            if (string_starts_with(line, markerDefEnd))
                break;

            for (std::pair<std::string, DefType> pair : markers) {
                if (string_starts_with(line, pair.first)) {
                    std::string title = string_trim(line.substr(pair.first.length()));
                    def = parse_def(title, pair.second, sourceLines, i, numLines);
                    break;
                }
            }

            // Assume it's a function
            if (def == nullptr) {
                std::string title = string_trim(line.substr(1));
                i++;
                def = parse_function_def(title, DEF_FUNCTION, sourceLines, i, numLines);
            }

            break;
        }

        if (def == nullptr || def->Title.size() == 0)
            continue;

        DocGroup* group = docLists[def->Type];
        group->DocList.push_back(def);
        group->Count++;

        allHref[def->getTitle()] = def->getHref();

        add_namespace(group, def);

        if (is_field_type(def->Type)) {
            group = docLists[DEF_FUNCTION];
            group->AddNamespace(def);
        }
    }
}

std::map<std::string, std::string> parse_desc_xml_tag_params(std::string input, size_t start, size_t end) {
    std::map<std::string, std::string> map;
    size_t i = start;

    do {
        while (isspace(input.at(i))) {
            if (++i >= end)
                return map;
        }

        size_t quoteStart = input.find("\"", i);
        if (quoteStart == std::string::npos)
            break;

        std::string tagParam = input.substr(i, quoteStart - i - 1);
        if (tagParam.size() < 1)
            break;

        size_t quoteEnd = input.find("\"", quoteStart + 1);
        if (quoteEnd == std::string::npos)
            break;

        std::string inside = input.substr(quoteStart + 1, (quoteEnd - 1) - quoteStart);
        map[tagParam] = inside;

        i = quoteEnd + 1;
    } while (i < end);

    return map;
}

std::string process_description(std::string input) {
    std::string linkToTag = "linkto";
    std::string replaceA, replaceB, linkTo;
    bool willReplace = false;
    bool foundLink = false;
    bool useCode = false;

    size_t tagStart = 0, tagEnd = 0;
    size_t i = 0;

    do {
        tagStart = input.find("<", i);
        if (tagStart == std::string::npos)
            break;

        tagEnd = input.find(">", tagStart + 1);
        if (tagEnd == std::string::npos)
            break;

        size_t contentStart = tagEnd + 1;

        // Check if this is a linkto tag
        size_t linkToPos = input.find(linkToTag, tagStart + 1);
        if (linkToPos != std::string::npos) {
            std::map<std::string, std::string> map = parse_desc_xml_tag_params(input, linkToPos + linkToTag.size(), tagEnd);
            if (map.count("ref") > 0) {
                std::string href;
                if (allHref.count(map["ref"]) > 0) {
                    linkTo = map["ref"];
                    href = allHref[linkTo];
                    foundLink = true;
                }
                else {
                    linkTo = map["ref"];
                    href = "";
                }

                replaceA = input.substr(0, tagStart);
                willReplace = true;
            }
        }

        tagStart = input.find("</", contentStart);
        if (tagStart == std::string::npos)
            break;

        tagEnd = input.find(">", tagStart + 1);
        if (tagEnd == std::string::npos)
            break;

        i = tagEnd + 1;

        if (willReplace) {
            std::string output;
            std::string contents = input.substr(contentStart, (tagStart - 1) - contentStart + 1);

            if (contents.size() == 0) {
                contents = linkTo;
                useCode = true;
            }

            if (useCode)
                replaceA += "<code>";
            if (foundLink)
                replaceA += "<a href=\"#" + allHref[linkTo] + "\">";

            replaceB = input.substr(tagEnd + 1);
            output = replaceA + contents;
            if (foundLink)
                output += "</a>";
            if (useCode)
                output += "</code>";

            i = output.size();
            input = output + replaceB;

            willReplace = false;
            foundLink = false;
            useCode = false;
        }
    } while (true);

    return input;
}

std::string write_docdef_title(DocDef* doc) {
    return "        <h3 style=\"margin-bottom: 8px;\"><code>" + doc->getTitle() + "</code></h2>\n";
}

std::string write_docdef_description(DocDef* doc) {
    std::string desc = process_description(doc->Description);
    return "        <div style=\"margin-top: 8px; font-size: 14px;\">" + desc + "</div>\n";
}

std::string write_docdef_type(ConstantDef* doc) {
    return "        <div style=\"font-size: 14px;\"><b>Type: </b>" + doc->ValueType + "</div>\n";
}

std::string write_function_docs(FunctionDef* func) {
    std::string docsHtml = "";

    unsigned i = 0;
    std::string parameter = "(";
    bool doOptEnd = false;
    for (ParamDef param : func->Params) {
        std::string label = param.Label.substr(0, param.Label.find('(') - 1);
        if (param.Optional && !doOptEnd) {
            parameter += "[";
            doOptEnd = true;
        }
        if (i == 0)
            parameter += label;
        else
            parameter += ", " + label;
        i++;
    }
    if (doOptEnd)
        parameter += "]";
    parameter += ")";

    // Function title
    if (func->Description != "")
        docsHtml += "        <h2 style=\"margin-bottom: 8px;\">" + func->getTitle() + "</h2>\n";
    else
        docsHtml += "        <h2 style=\"margin-bottom: 8px; color: red;\">" + func->getTitle() + "</h2>\n";
    // Function parameters
    docsHtml += "        <code>" + func->getTitle() + parameter + "</code>\n";
    // Function description
    if (func->Description != "")
        docsHtml += write_docdef_description(func);
    // Parameter descriptions
    if (func->Params.size() > 0) {
        docsHtml += "        <div style=\"font-weight: bold; margin-top: 8px;\">Parameters:</div>\n";
        docsHtml += "        <ul style=\"margin-top: 0px; font-size: 14px;\">\n";
        for (ParamDef param : func->Params)
            docsHtml += "        <li>" + process_description(param.Label) + "</li>\n";
        docsHtml += "        </ul>\n";
    }
    // Return description
    if (func->Returns != "") {
        docsHtml += "        <div style=\"font-weight: bold; margin-top: 8px;\">Returns:</div>\n";
        docsHtml += "        <div style=\"font-size: 14px;\">" + process_description(func->Returns) + "</div>\n";
    }

    return docsHtml;
}

std::string write_generic_docs(DocDef* doc) {
    std::string docsHtml = write_docdef_title(doc);

    // Description
    if (doc->Description != "")
        docsHtml += write_docdef_description(doc);

    return docsHtml;
}

std::string write_constant_docs(ConstantDef* doc) {
    std::string docsHtml = write_docdef_title(doc);

    // Type
    if (doc->ValueType != "")
        docsHtml += write_docdef_type(doc);
    // Description
    if (doc->Description != "")
        docsHtml += write_docdef_description(doc);

    return docsHtml;
}

std::string write_field_docs(FieldDef* doc) {
    std::string docsHtml = write_docdef_title(doc);

    // Type
    if (doc->ValueType != "")
        docsHtml += write_docdef_type(doc);
    // Default value
    if (doc->DefaultValue != "")
        docsHtml += "        <div style=\"font-size: 14px;\"><b>Default: </b><code>" + doc->DefaultValue + "</code></div>\n";
    // Description
    if (doc->Description != "")
        docsHtml += write_docdef_description(doc);

    return docsHtml;
}

std::string get_namespace_title(DefType type) {
    if (type == DEF_FUNCTION)
        return "Namespaces";

    return std::string(defTypeNames[type][1]);
}

bool can_write_namespace_link_list(DefType type) {
    if (is_field_type(type))
        return false;

    if (type == DEF_FUNCTION || type == DEF_METHOD || type == DEF_ENUM) {
        if (!docLists[type]->NamespaceList.size())
            return false;
    }

    if (!docLists[type]->DocList.size())
        return false;

    return true;
}

std::string write_namespace_link_list(DefType type) {
    DocGroup* group = docLists[type];

    std::string docsHtml = "";

    docsHtml += "        <h3>" + get_namespace_title(type) + "</h3>\n";
    docsHtml += "        <ul>\n";

    if (type == DEF_FUNCTION || type == DEF_METHOD || type == DEF_ENUM) {
        // Sort namespace list alphabetically
        std::sort(group->NamespaceList.begin(), group->NamespaceList.end());

        for (std::string nsName : group->NamespaceList) {
            if (type == DEF_ENUM && !allNamespaces[nsName]->IsEnumNamespace)
                continue;
            docsHtml += "            <li><a href=\"#" + getNamespaceHref(nsName) + "\">" + nsName + "</a></li>\n";
        }
    }
    else {
        for (DocDef* doc : group->DocList)
            docsHtml += "                    <li><a href=\"#" + doc->getHref() + "\">" + doc->getTitle() + "</a></li>\n";
    }

    return docsHtml + "        </ul>\n";
}

bool can_write_namespace_contents_list(DefType type) {
    return can_write_namespace_link_list(type);
}

std::string write_enum_namespace_contents_list() {
    DocGroup* group = docLists[DEF_ENUM];

    std::string docsHtml = "";

    docsHtml += "        <h3>" + std::string(defTypeNames[DEF_ENUM][1]) + "</h3>\n";

    for (std::string nsName : group->NamespaceList) {
        NamespaceInfo* nsInfo = allNamespaces[nsName];
        if (!nsInfo->IsEnumNamespace)
            continue;

        docsHtml += "            <p id=\"" + getNamespaceHref(nsName) + "\">\n";
        docsHtml += "                <h2><code>" + nsName + "</code></h2>\n";

        if (!nsInfo->DocsPerDef[DEF_ENUM].size())
            break;

        docsHtml += "                <ul>\n";
        for (DocDef* doc : nsInfo->DocsPerDef[DEF_ENUM])
            docsHtml += "                    <li><a href=\"#" + doc->getHref() + "\">" + doc->getTitle() + "</a></li>\n";
        docsHtml += "                </ul>\n";
        docsHtml += "            </p>\n";
    }

    return docsHtml;
}

bool can_write_docs(DefType type) {
    DocGroup* group = docLists[type];
    return group->Count > 0;
}

std::string write_docdef(DocGroup* group, DocDef* doc, DefType type) {
    std::string docsHtml = "        <p id=\"" + doc->getHref() + "\">\n";

    switch (type) {
    case DEF_FUNCTION:
    case DEF_METHOD: {
        FunctionDef* funcDef = static_cast<FunctionDef*>(doc);
        docsHtml += write_function_docs(funcDef);
        break;
    }
    case DEF_CONSTANT: {
        ConstantDef* constantDef = static_cast<ConstantDef*>(doc);
        docsHtml += write_constant_docs(constantDef);
        break;
    }
    case DEF_FIELD:
    case DEF_CLASS_FIELD: {
        FieldDef* fieldDef = static_cast<FieldDef*>(doc);
        docsHtml += write_field_docs(fieldDef);
        break;
    }
    default:
        docsHtml += write_generic_docs(doc);
        break;
    }

    if (doc->Description != "")
        group->HasDesc++;

    return docsHtml + "        </p>\n";
}

std::string write_docs(DefType type) {
    DocGroup* group = docLists[type];

    std::string docsHtml = "";

    docsHtml += "        <h3>" + std::string(defTypeNames[type][1]) + "</h3>\n";

    if (type == DEF_CONSTANT || type == DEF_GLOBAL_VAR) {
        for (DocDef* doc : group->DocList)
            docsHtml += write_docdef(group, doc, type);
    }
    else {
        for (std::string nsName : group->NamespaceList) {
            NamespaceInfo* nsInfo = allNamespaces[nsName];

            for (DocDef* doc : nsInfo->DocsPerDef[type])
                docsHtml += write_docdef(group, doc, type);
        }
    }

    docsHtml += "        <p>"
        + std::to_string(group->HasDesc)
        + " out of " + std::to_string(group->Count)
        + " " + defTypeNames[type][0] + " have descriptions. </p>\n";

    docsHtml += "        <hr/>\n";

    return docsHtml;
}

std::string write_namespace_contents_list(DefType type) {
    if (type == DEF_ENUM)
        return write_enum_namespace_contents_list();

    DocGroup* group = docLists[type];

    std::string docsHtml = "";

    docsHtml += "        <h3>" + std::string(defTypeNames[type][1]) + "</h3>\n";

    for (std::string nsName : group->NamespaceList) {
        NamespaceInfo* nsInfo = allNamespaces[nsName];
        docsHtml += "            <p id=\"" + getNamespaceHref(nsName) + "\">\n";
        docsHtml += "                <h2>" + nsName + "</h2>\n";
        for (size_t i = 0; i < DEF_LAST; i++) {
            if (!nsInfo->DocsPerDef[i].size())
                continue;
            docsHtml += "                <i>" + std::string(defTypeNames[i][1]) + ":</i>\n";
            docsHtml += "                <ul>\n";
            for (DocDef* doc : nsInfo->DocsPerDef[i])
                docsHtml += "                    <li><a href=\"#" + doc->getHref() + "\">" + doc->getTitle() + "</a></li>\n";
            docsHtml += "                </ul>\n";
        }
        docsHtml += "            </p>\n";
    }

    return docsHtml;
}

void generate_doc_file(std::string output_file) {
    std::string docsHtml = "";

    const char* lines[] = {
        "<html>\n",

        "    <head>\n",
        "        <title>Hatch Game Engine Documentation</title>\n",
        "        <style>\n",
        "            body {\n",
        "                background-color: white;\n",
        "                font-family: sans-serif;\n",
        "                margin: 64px;\n",
        "            }\n",
        "            codefrag {\n",
        "                display: inline;\n",
        "                margin: 0px;\n",
        "                font-family: monospace;\n",
        "            }\n",
        "            a {\n",
        "                text-decoration: none;\n",
        "                color: #4141F2;\n",
        "            }\n",
        "            .function_list {\n",
        "                font-family: monospace;\n",
        "                margin-top: 0.5em;\n",
        "            }\n",
        "            .function_list li {\n",
        "                margin-top: 0.125em;\n",
        "                margin-bottom: 0.125em;\n",
        "            }\n",
        "            code, pre.code {\n",
        "                background-color: #f2f2f2;\n",
        "                border-radius: 3px;\n",
        "                padding: 3px;\n",
        "            }\n",
        "            codeBlock {\n",
        "                background-color: #f2f2f2;\n",
        "                border-radius: 3px;\n",
        "                padding: 3px;\n",
        "                line-height: 100%;\n",
        "                word-break: normal;\n",
        "                font-family: monospace;\n",
        "            }\n",
        "        </style>\n",
        "    </head>\n",

        "    <body>\n",
        "        <div style=\"position: fixed; margin-top: -32px; margin-left: -96px; width: 100%; text-align: right; \">\n",
        "            <a href=\"#Reference_top\">Back to top</a>\n",
        "        </div>\n",
        "        <h1 id=\"Reference_top\">Hatch Game Engine Reference</h1>\n"
    };
    for (const char* line : lines)
        docsHtml += line;

    // Write out all namespaces
    for (unsigned i = DEF_FUNCTION; i < DEF_LAST; i++) {
        if (can_write_namespace_link_list((DefType)i))
            docsHtml += write_namespace_link_list((DefType)i);
    }

    docsHtml += "        <hr/>\n";

    // Write out what's in those namespaces
    for (unsigned i = DEF_FUNCTION; i < DEF_LAST; i++) {
        if (can_write_namespace_contents_list((DefType)i))
            docsHtml += write_namespace_contents_list((DefType)i);
    }

    docsHtml += "        <hr/>\n";

    // Write out docs
    for (unsigned i = DEF_FUNCTION; i < DEF_LAST; i++) {
        if (can_write_docs((DefType)i))
            docsHtml += write_docs((DefType)i);
    }

    docsHtml += "    </body>\n";
    docsHtml += "</html>\n";

    std::ofstream out(output_file);
    if (out) {
        out << docsHtml;
        out.close();
    } else
        fprintf(stderr, "couldn't output to %s\n", output_file.c_str());
}

void prepare() {
    for (size_t i = 0; i < DEF_LAST; i++)
        docLists.push_back(new DocGroup());
}

const char* files[] = {
    "/Engine/Bytecode/StandardLibrary.cpp",
    "/Engine/Bytecode/ScriptEntity.cpp"
};

void read_files(std::string source_folder) {
    for (const char* file : files)
        read_file(source_folder + std::string(file));
}

int main(int argc, char **argv) {
    std::string source_folder = "", output_file = "";

    if (argc >= 2) {
        source_folder = std::string(argv[1]);
        if (argc >= 3)
            output_file = std::string(argv[2]);
    }

    if (source_folder == "")
        source_folder = "../source";
    if (output_file == "")
        output_file = "../guides/Documentation.htm";

    if (source_folder == "--usage" || !source_folder.size() || !output_file.size()) {
        fprintf(stderr, "%s\n", "usage: docgen <source path> <output file>");
        return EXIT_FAILURE;
    }

    prepare();

    read_files(source_folder);
    generate_doc_file(output_file);

    return EXIT_SUCCESS;
}
